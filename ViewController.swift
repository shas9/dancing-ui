//
//  ViewController.swift
//  Dancing UI
//
//  Created by Kite Games Studio on 31/8/21.
//

import UIKit

class ViewController: UIViewController {


    @IBOutlet weak var rectangleView: UIView!
    @IBOutlet weak var rectangleViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var rectangleViewTrailingContraint: NSLayoutConstraint!
    @IBOutlet weak var clickMe: UIButton!
    @IBOutlet weak var panelStackView: UIStackView!
    @IBOutlet weak var rectangleViewCenterX: NSLayoutConstraint!
    @IBOutlet weak var rectangleViewWidth: NSLayoutConstraint!
    @IBOutlet weak var rectangleViewHeight: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        print("View did load called")
        self.clickMe.isEnabled = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        print("ViewDidAppear Called")
        
        panelStackView.frame.origin.y = self.view.frame.size.height
//        panelStackView.transform = CGAffineTransform(scaleX: 0.01, y: 1)
    }
    
    
    

    @IBAction func tapOnView(_ sender: UITapGestureRecognizer) {
        
        print("Tap on view called: click me button is \(self.clickMe.isEnabled) and view width = \(self.view.frame.size.width)")
        print(self.rectangleView.frame.origin.x)
      
        var tempFrame = self.rectangleView.frame
        tempFrame.origin.x = self.clickMe.isEnabled ? -50 : (self.view.frame.size.width - 50)
        print(tempFrame.origin.x)
        
        UIView.animate(withDuration: 0.6, animations: {
            self.rectangleView.frame = tempFrame
            print(self.rectangleView.frame.origin.x)
        }, completion: { _ in
            self.clickMe.isEnabled = !self.clickMe.isEnabled
            self.panelStackView.frame.origin.y = self.view.frame.size.height
//            self.panelStackView.transform = CGAffineTransform(scaleX: 0.01, y: 1)
        })
        print(self.rectangleView.frame.origin.x)
        
    }
    
    
    @IBAction func clickMeButton(_ sender: Any) {
        
        print("Click me button called")
        
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 10, animations: {
            self.rectangleView.frame.origin.x = ( self.view.frame.size.width / 2 ) - 50
            self.panelStackView.frame.origin.y = self.view.frame.size.height - 150
//            self.panelStackView.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    @IBAction func dance360Button(_ sender: Any) {
        
        print("dance 360 called")
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, animations: {
            self.rectangleView.transform = CGAffineTransform(rotationAngle: .pi + .pi / 6)
        }, completion: { _ in
            
        })
        
        UIView.animate(withDuration: 0.5, delay: 2, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, animations: {
            self.rectangleView.transform = CGAffineTransform(rotationAngle: 0)
            print("Rotation Done")
            self.panelStackView.frame.origin.y = self.view.frame.size.height
//            self.panelStackView.transform = CGAffineTransform(scaleX: 0.01, y: 1)
        }, completion: { _ in
            
//            self.panelStackView.transform = CGAffineTransform(scaleX: 0.01, y: 1)
        })
    }
    
    @IBAction func make2xButton(_ sender: Any) {
        
        print("make 2x called")
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, animations: {
            self.rectangleView.transform = CGAffineTransform(scaleX: 2, y: 2)
        }, completion: { _ in
            
//            self.panelStackView.transform = CGAffineTransform(scaleX: 0.01, y: 1)
        })
        
        UIView.animate(withDuration: 0.5, delay: 2, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, animations: {
            self.rectangleView.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.panelStackView.frame.origin.y = self.view.frame.size.height
        }, completion: { _ in
            
//            self.panelStackView.transform = CGAffineTransform(scaleX: 0.01, y: 1)
        })
    }
}

